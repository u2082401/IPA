package logic;

import ch.ehi.bl.iox2geom.Converter;
import ch.ehi.bl.iox2geom.ConverterException;
import ch.interlis.iom.IomObject;
import static ch.interlis.iox_j.jts.Iox2jts.surface2JTS;
import ch.interlis.iox_j.jts.Iox2jtsException;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.MultiLineString;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.operation.distance.DistanceOp;
import geometry.Distance;
import geometry.GArea;
import geometry.GBorder;
import geometry.GLine;
import geometry.GPoint;
import geometry.Geom;
import geometry.LXL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * performs all the geometry tests
 *
 * @author schlatter
 */
public class GeometryTests {

    private Map<String, IomObject> polyMap;
    private Map<String, IomObject> polyWArcMap;
    private boolean lineGeom;
    private boolean polyGeom;
    private boolean wArc;
    private int numTestObjects;
    private int coordsX;
    private int coordsY;
    private int countBl = 0;
    private int countJts = 0;
    private int countAll = 0;
    private List<Map<String, List<String>>> listLine = new ArrayList<Map<String, List<String>>>();
    private List<Map<String, List<String>>> listPoly = new ArrayList<Map<String, List<String>>>();
    private List<Map<String, List<String>>> listPolyWArc = new ArrayList<Map<String, List<String>>>();
    //geomType is to differentiate between the geometries to save them 
    private final int geomTypeLine = 1;
    private final int geomTypePoly = 2;
    private final int geomTypePolyWArc = 3;
    private String objectId;
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
    private final String logStart = "Start";
    private final String logEnd = "Finished successfully and saved to list";
    //boolean used to allow the test for the specific coordinate entered through user
    private boolean allowTests = true;

    /**
     *
     * @param lineGeom
     * @param polyGeom
     * @param wArc
     * @param numTestObjects
     * @param coordsX
     * @param coordsY
     * @param polyMap
     * @param polyWArcMap
     */
    public GeometryTests(boolean lineGeom,
            boolean polyGeom,
            boolean wArc,
            String numTestObjects,
            String coordsX,
            String coordsY,
            Map<String, IomObject> polyMap,
            Map<String, IomObject> polyWArcMap) {

        this.lineGeom = lineGeom;
        this.polyGeom = polyGeom;
        this.wArc = wArc;
        this.polyMap = polyMap;
        this.numTestObjects = Integer.parseInt(numTestObjects);

        if (!(coordsX.equals("") && coordsY.equals(""))) {
            this.coordsX = Integer.parseInt(coordsX);
            this.coordsY = Integer.parseInt(coordsY);
        }

        // check if wArc Checkbox is selected
        if (this.wArc == true) {
            this.polyWArcMap = polyWArcMap;
        }
    }

    /**
     * starts all geometry tests
     */
    public void start() {
        //fictive points for testMethods
        Point fictivePoint = new GeometryFactory().createPoint(new Coordinate(10, 10));
        GPoint fictiveGPoint = new GPoint("", 10, 10);

        //int to count up for numTestObjects
        int count = 0;

        for (Map.Entry<String, IomObject> entry : this.polyMap.entrySet()) {
            this.objectId = entry.getKey();

            if (count < numTestObjects) {

                //Checks if there are coords entered
                //if there are it doesn't count up on the first one because it has to search through the objects
                if (this.coordsX == 0) {
                    count++;
                } else {
                    this.allowTests = false;
                }

                // int to count all created objects
                countAll++;

                try {
                    Polygon poly = surface2JTS(entry.getValue(), 0);
                    this.countJts++;
                    GArea ga = Converter.surface2garea(entry.getValue());
                    this.countBl++;

                    //checks if the entered coordinates is in polygon
                    if (poly.contains(new GeometryFactory().createPoint(new Coordinate(this.coordsX, this.coordsY)))) {
                        count++;
                        this.allowTests = true;
                    }

                    if (this.allowTests == true) {
                        if (this.lineGeom) {
                            LOGGER.info("Start line geometry tests");
                            //create JTS LineString
                            Coordinate[] coordinates = poly.getCoordinates();
                            Coordinate[] coordinatesLine = new Coordinate[]{coordinates[0], coordinates[1]};
                            LineString line = new GeometryFactory().createLineString(coordinatesLine);

                            //Create BL GLine
                            GBorder border = (ga.getBorder(0));
                            GPoint[] points = border.getPointVector();
                            GPoint p1 = points[0];
                            GPoint p2 = points[1];
                            GLine gLine = new GLine(p1, p2);

                            this.logCoordinateInfos(line, gLine, fictivePoint, fictiveGPoint);

                            // all test methods
                            this.lineIsValid(this.geomTypeLine, line, gLine);
                            this.lineLength(this.geomTypeLine, line, gLine);
                            this.lineDistanceToPoint(this.geomTypeLine, line, gLine, fictivePoint, fictiveGPoint);
                            this.lineCheckMbr(this.geomTypeLine, line, gLine);
                            this.lineIntersectLine(this.geomTypeLine, line, gLine);
                            this.lineIntersectPoint(this.geomTypeLine, line, gLine);
                            this.lineIntersectPointOutside(this.geomTypeLine, line, gLine, fictivePoint, fictiveGPoint);
                            this.lineDetermineNearestVertex(this.geomTypeLine, line, gLine);
                        }

                        if (this.polyGeom) {
                            LOGGER.info("Start polygon geometry tests");

                            this.logCoordinateInfos(poly, ga, fictivePoint, fictiveGPoint);

                            this.polyIsValid(this.geomTypePoly, poly, ga);
                            this.polyArea(this.geomTypePoly, poly, ga, "");
                            this.polyCheckMbr(this.geomTypePoly, poly, ga);
                            this.polyIntersectLine(this.geomTypePoly, poly, ga);
                            this.polyIntersectPoint(this.geomTypePoly, poly, ga, fictivePoint, fictiveGPoint);
                            this.polyDetermineNearestVertex(this.geomTypePoly, poly, ga);
                            this.polyCheckCentroidInArea(this.geomTypePoly, poly, ga);
                            this.polyCountAreaObjects(this.geomTypePoly, poly, ga);
                            this.polyCountVertex(this.geomTypePoly, poly, ga);
                        }
                    }

                } catch (Iox2jtsException | ConverterException ex) {
                    ex.printStackTrace();
                    LOGGER.severe("There was an error with the creation of the polygons or/and the line geometries.");
                    LOGGER.severe(ex.getLocalizedMessage());
                }
            }
        }

        //all counters reset
        count = 0;
        countAll = 0;
        countJts = 0;
        countBl = 0;

        if (this.wArc) {
            for (Map.Entry<String, IomObject> entry : this.polyWArcMap.entrySet()) {
                this.objectId = entry.getKey();

                if (count < numTestObjects) {

                    //Checks if there are coords entered
                    //if there are it doesn't count up on the first one because it has to search through the objects
                    if (this.coordsX == 0) {
                        count++;
                    } else {
                        this.allowTests = false;
                    }

                    countAll++;

                    try {
                        Polygon poly = surface2JTS(entry.getValue(), 0);
                        Polygon polyWStroke = surface2JTS(entry.getValue(), 0.001);
                        //counts up if poly is created because if it has an error the exception activates and it wont be added 
                        this.countJts++;

                        GArea ga = Converter.surface2garea(entry.getValue());
                        GArea gaWStroke = Converter.surface2garea(entry.getValue());
                        gaWStroke.arcsToStrokedLine(0.001);
                        this.countBl++;

                        //checks if the entered coordinates is in polygon
                        if (poly.contains(new GeometryFactory().createPoint(new Coordinate(this.coordsX, this.coordsY)))) {
                            count++;
                            this.allowTests = true;
                        }

                        if (this.allowTests == true) {
                            LOGGER.info("Start polygon with arc geometry tests");

                            this.logCoordinateInfos(poly, ga, null, null);
                            LOGGER.info("The following coordinates are from polygon with stroke geometries");
                            this.logCoordinateInfos(polyWStroke, gaWStroke, fictivePoint, fictiveGPoint);

                            // polywArc test methods
                            this.polyArea(this.geomTypePolyWArc, poly, ga, "Polygon Fläche");
                            this.polyArea(this.geomTypePolyWArc, polyWStroke, gaWStroke, "Polygon Fläche mit Stroke 0.001m^2");
                            this.polyCountAreaObjects(this.geomTypePolyWArc, poly, ga);
                            this.polyCountVertex(this.geomTypePolyWArc, polyWStroke, ga);
                        }

                    } catch (Iox2jtsException | ConverterException ex) {
                        ex.printStackTrace();
                        LOGGER.severe("There was an error with the creation of the polygons with arc geometries.");
                        LOGGER.severe(ex.getLocalizedMessage());
                    }
                }
            }
        }
    }

    /**
     * Checks if the geometry is topologically valid, according to the OGC SFS
     * specification
     *
     * @param geomType
     * @param lineString
     * @param gLine
     */
    public void lineIsValid(int geomType, LineString lineString, GLine gLine) {
        LOGGER.log(Level.INFO, this.logStart);

        //no method to check if GLine is valid
        boolean jtsValid = lineString.isValid();

        String testName = "Linien Validitaet";
        String jtsResult;
        String blResult = "-";
        boolean comparison = false;

        if (jtsValid) {
            jtsResult = "Valid";
        } else {
            jtsResult = "Unvalid";
        }

        this.output(geomType, testName, jtsResult, blResult, comparison);
    }

    /**
     * checks if the length of the line is the same
     *
     * @param geomType
     * @param lineString
     * @param gLine
     */
    public void lineLength(int geomType, LineString lineString, GLine gLine) {
        LOGGER.log(Level.INFO, this.logStart);

        double jtsLength = lineString.getLength();
        double blLength = gLine.getLength();

        String testName = "Gleiche Länge";
        String jtsResult = Double.toString(jtsLength);
        String blResult = Double.toString(blLength);
        boolean comparison;

        if (jtsLength == blLength) {
            comparison = true;
        } else {
            comparison = false;
        }

        this.output(geomType, testName, jtsResult, blResult, comparison);
    }

    /**
     *
     * checks if the distance to a point is the same
     *
     * @param geomType
     * @param lineString
     * @param gLine
     * @param fictivePoint
     * @param fictiveGPoint
     */
    public void lineDistanceToPoint(int geomType, LineString lineString, GLine gLine, Point fictivePoint, GPoint fictiveGPoint) {
        LOGGER.log(Level.INFO, this.logStart);

        double jtsDistance = lineString.distance(fictivePoint);
        //double blDistance = Distance.signedDistToStraight(fictiveGPoint, gLine.getStart(), gLine.getEnd());
        double blDistance = Distance.dist(fictiveGPoint, gLine.getStart());

        String testName = "Gleiche Distanz zu Punkt";
        String jtsResult = Double.toString(jtsDistance);
        String blResult = Double.toString(blDistance);
        boolean comparison;

        if (jtsDistance == blDistance) {
            comparison = true;
        } else {
            comparison = false;
        }

        this.output(geomType, testName, jtsResult, blResult, comparison);

    }

    /**
     * Checks if the MBR (Minimum Bounding Rectangle) is the same
     *
     * @param geomType
     * @param lineString
     * @param gLine
     */
    public void lineCheckMbr(int geomType, LineString lineString, GLine gLine) {
        LOGGER.log(Level.INFO, this.logStart);

        double jtsMaxX = lineString.getEnvelopeInternal().getMaxX();
        double jtsMaxY = lineString.getEnvelopeInternal().getMaxY();
        double jtsMinX = lineString.getEnvelopeInternal().getMinX();
        double jtsMinY = lineString.getEnvelopeInternal().getMinY();

        double blMaxX = gLine.xMax();
        double blMaxY = gLine.yMax();
        double blMinX = gLine.xMin();
        double blMinY = gLine.yMin();

        boolean compareMaxX = jtsMaxX == blMaxX;
        boolean compareMaxY = jtsMaxY == blMaxY;
        boolean compareMinX = jtsMinX == blMinX;
        boolean compareMinY = jtsMinY == blMinY;

        String testName = "Line MBR";
        String jtsResult = "xMin: " + jtsMinX + " xMax: " + jtsMaxX + " yMin: " + jtsMinY + " yMax: " + jtsMaxY;
        String blResult = "xMin: " + blMinX + " xMax: " + blMaxX + " yMin: " + blMinY + " yMax: " + blMaxY;
        boolean comparison;

        if (compareMaxX && compareMaxY && compareMinX && compareMinY) {
            comparison = true;
        } else {
            comparison = false;
        }
        this.output(geomType, testName, jtsResult, blResult, comparison);

    }

    /**
     * checks if the intersection point is the same
     *
     * @param geomType
     * @param lineString
     * @param gLine
     */
    public void lineIntersectLine(int geomType, LineString lineString, GLine gLine) {
        LOGGER.log(Level.INFO, this.logStart);
        //create the fictive lines to intersect with
        Coordinate[] lineCoords = new Coordinate[]{new Coordinate(lineString.getEndPoint().getX(), lineString.getStartPoint().getY()), new Coordinate(lineString.getStartPoint().getX(), lineString.getEndPoint().getY())};
        LineString fictiveLine = new GeometryFactory().createLineString(lineCoords);
        GLine fictiveGLine = new GLine(new GPoint("a", gLine.getEnd().x(), gLine.getStart().y()), new GPoint("b", gLine.getStart().x(), gLine.getEnd().y()));

        Point jtsIntersectionPoint = (Point) fictiveLine.intersection(lineString);

        //calculates intersection point
        GPoint blIntersectionPoint = this.blCutLine(fictiveGLine.getStart(), fictiveGLine.getEnd(), gLine.getStart(), gLine.getEnd());

        double jtsIntersectionX = jtsIntersectionPoint.getX();
        double jtsIntersectionY = jtsIntersectionPoint.getY();
        double blIntersectionX = blIntersectionPoint.x();
        double blIntersectionY = blIntersectionPoint.y();

        String testName = "Linie mit Linie gleicher Schnittpunkt";
        String jtsResult = "X: " + jtsIntersectionX + " Y: " + jtsIntersectionY;
        String blResult = "X: " + blIntersectionX + " Y: " + blIntersectionY;
        boolean comparison;

        if (jtsIntersectionX == blIntersectionX && jtsIntersectionY == blIntersectionY) {
            comparison = true;
        } else {
            comparison = false;
        }

        this.output(geomType, testName, jtsResult, blResult, comparison);
    }

    /**
     * checks if the intersection point is the same
     *
     * @param geomType
     * @param lineString
     * @param gLine
     */
    public void lineIntersectPoint(int geomType, LineString lineString, GLine gLine) {
        LOGGER.log(Level.INFO, this.logStart);
        //create fictivePoint
        Point fictivePoint = new GeometryFactory().createPoint(new Coordinate(((lineString.getStartPoint().getX() + lineString.getEndPoint().getX()) / 2), ((lineString.getStartPoint().getY() + lineString.getEndPoint().getY()) / 2)));
        GPoint fictiveGPoint = new GPoint("", ((gLine.getStart().x() + gLine.getEnd().x()) / 2), ((gLine.getStart().y() + gLine.getEnd().y()) / 2));

        Point jtsIntersectionPoint = (Point) lineString.intersection(fictivePoint);

        //defining second line for test as point to get intersection point
        //calculates intersection point
        GPoint blIntersectionPoint = this.blCutLine(gLine.getStart(), gLine.getEnd(), fictiveGPoint, fictiveGPoint);

        String testName = "Linie mit Punkt gleicher Schnittpunkt";;
        String jtsResult;
        String blResult;
        boolean comparison;

        if (!jtsIntersectionPoint.isEmpty() && blIntersectionPoint != null) {
            double jtsIntersectionX = jtsIntersectionPoint.getX();
            double jtsIntersectionY = jtsIntersectionPoint.getY();
            double blIntersectionX = blIntersectionPoint.x();
            double blIntersectionY = blIntersectionPoint.y();

            jtsResult = "X: " + jtsIntersectionX + " Y: " + jtsIntersectionY;
            blResult = "X: " + blIntersectionX + " Y: " + blIntersectionY;

            if (jtsIntersectionX == blIntersectionX && jtsIntersectionY == blIntersectionY) {
                comparison = true;
            } else {
                comparison = false;
            }
        } else {
            jtsResult = "Kein Schnittpunkt";
            blResult = "Kein Schnittpunkt";
            comparison = true;
        }
        this.output(geomType, testName, jtsResult, blResult, comparison);

    }

    /**
     * line tests if there is an intersection with a point outside
     *
     * @param geomType
     * @param lineString
     * @param gLine
     * @param fictivePoint
     * @param fictiveGPoint
     */
    public void lineIntersectPointOutside(int geomType, LineString lineString, GLine gLine, Point fictivePoint, GPoint fictiveGPoint) {
        LOGGER.log(Level.INFO, this.logStart);

        //defining second line for test as point to get intersection point
        GPoint[] resultArray = new GPoint[2];
        GPoint s1 = gLine.getStart();
        GPoint e1 = gLine.getEnd();
        GPoint s2 = fictiveGPoint;
        GPoint e2 = fictiveGPoint;

        //calculates intersection point
        int blIntersectionInt = LXL.straightXstraight(resultArray, s1, e1, s2, e2, 0.0);
        boolean jtsIntersection = fictivePoint.intersects(lineString);
        boolean blIntersection;
        String testName = "Schnittpunkt Test mit Punkt nicht auf der Linie";
        String jtsResult;
        String blResult;
        boolean comparison;

        if (blIntersectionInt == -1) {
            blIntersection = false;
            blResult = "Kein Schnittpunkt";
        } else {
            blIntersection = true;
            blResult = "Schnittpunkt vorhanden";
        }

        if (jtsIntersection == false) {
            jtsResult = "Kein Schnittpunkt";
        } else {
            jtsResult = "Schnittpunkt vorhanden";
        }

        if (jtsIntersection == blIntersection) {
            comparison = true;
        } else {
            comparison = false;
        }

        this.output(geomType, testName, jtsResult, blResult, comparison);

    }

    /**
     * checks for the nearest Vertex
     *
     * @param geomType
     * @param lineString
     * @param gLine
     */
    public void lineDetermineNearestVertex(int geomType, LineString lineString, GLine gLine) {
        LOGGER.log(Level.INFO, this.logStart);
        //create fictive points the check for the nearest Vertex
        Point point = new GeometryFactory().createPoint(new Coordinate(((lineString.getStartPoint().getX() + lineString.getEndPoint().getX()) / 2), ((lineString.getStartPoint().getY() + lineString.getEndPoint().getY()) / 2)));
        GPoint gPoint = new GPoint("a", ((gLine.getStart().x() + gLine.getEnd().x()) / 2), ((gLine.getStart().y() + gLine.getEnd().y()) / 2));

        //compute nearest vertex
        Coordinate[] jtsVertex = DistanceOp.nearestPoints(point, lineString);
        GPoint blVertex = gLine.getNearestVertex(gPoint, 0);

        double jtsVertexX = jtsVertex[0].x;
        double jtsVertexY = jtsVertex[0].y;
        double blVertexX = blVertex.x();
        double blVertexY = blVertex.y();

        String testName = "Linie nächster Stützpunkt";
        String jtsResult = "x: " + jtsVertexX + " " + "y: " + jtsVertexY;
        String blResult = "x: " + blVertexX + " " + "y: " + blVertexY;
        boolean comparison;

        if (jtsVertexX == blVertexX && jtsVertexY == blVertexY) {
            comparison = true;
        } else {
            comparison = false;
        }

        this.output(geomType, testName, jtsResult, blResult, comparison);
        //Create new resultList for the next lineobjecttests
        //this.resultListLine = new ArrayList<String>();
    }

    /**
     * checks if polygon is valid
     *
     * @param geomType
     * @param poly
     * @param ga
     */
    public void polyIsValid(int geomType, Polygon poly, GArea ga) {
        LOGGER.log(Level.INFO, this.logStart);

        boolean jtsValid = poly.isValid();
        boolean blValid = ga.getBorder(0).isValid();

        String testName = "Polygon Validität";
        String jtsResult;
        String blResult;
        boolean comparison;

        if (jtsValid) {
            jtsResult = "Valid";
        } else {
            jtsResult = "Unvalid";
        }

        if (blValid) {
            blResult = "Valid";
        } else {
            blResult = "Unvalid";
        }

        if (jtsValid == blValid) {
            comparison = true;
        } else {
            comparison = false;
        }
        this.output(geomType, testName, jtsResult, blResult, comparison);
    }

    /**
     * counts if the created objects are the same for both libraries
     *
     * @param geomType
     * @param poly
     * @param ga
     */
    public void polyCountAreaObjects(int geomType, Polygon poly, GArea ga) {

        if (countAll == this.numTestObjects) {
            LOGGER.log(Level.INFO, this.logStart);

            String testName = "Anzahl Flächenobjekte";
            String jtsResult = Integer.toString(countJts);
            String blResult = Integer.toString(countBl);
            boolean comparison;

            if (countJts == countBl) {
                comparison = true;
            } else {
                comparison = false;
            }
            this.output(geomType, testName, jtsResult, blResult, comparison);
        }
    }

    /**
     * checks if the area is the same
     *
     * @param geomType
     * @param poly
     * @param ga
     * @param name param for testName because of the 2 tests with polyWArc with
     * stroke and without stroke
     */
    public void polyArea(int geomType, Polygon poly, GArea ga, String name) {
        LOGGER.log(Level.INFO, this.logStart);

        double jtsArea = poly.getArea();
        double blArea = ga.getArea();

        String testName = "Polygon Fläche";
        String jtsResult = Double.toString(jtsArea);
        String blResult = Double.toString(blArea);
        boolean comparison = false;

        //for polygon
        switch (geomType) {
            case 2:
                if (jtsArea == blArea) {
                    comparison = true;
                } else {
                    comparison = false;
                }
                //for polyWArc objects
                break;
            case 3:
                testName = name;
                //tolerance for area values
                if ((jtsArea + 0.1) >= blArea && (jtsArea - 0.1) <= blArea) {
                    comparison = true;
                } else {
                    comparison = false;
                }
                break;
            default:
                LOGGER.log(Level.WARNING, "geomType is wrong. It should be number 2 or 3. geomType: {0}", geomType);
                break;
        }

        this.output(geomType, testName, jtsResult, blResult, comparison);
    }

    /**
     * checks if the MBR (Minimum Bounding Rectangle) is the same
     *
     * @param geomType
     * @param poly
     * @param ga
     */
    public void polyCheckMbr(int geomType, Polygon poly, GArea ga) {
        LOGGER.log(Level.INFO, this.logStart);

        double jtsMaxX = poly.getEnvelopeInternal().getMaxX();
        double jtsMaxY = poly.getEnvelopeInternal().getMaxY();
        double jtsMinX = poly.getEnvelopeInternal().getMinX();
        double jtsMinY = poly.getEnvelopeInternal().getMinY();

        double blMaxX = ga.xMax();
        double blMaxY = ga.yMax();
        double blMinX = ga.xMin();
        double blMinY = ga.yMin();

        boolean compareMaxX = jtsMaxX == blMaxX;
        boolean compareMaxY = jtsMaxY == blMaxY;
        boolean compareMinX = jtsMinX == blMinX;
        boolean compareMinY = jtsMinY == blMinY;

        String testName = "Polygon MBR";
        String jtsResult = "xMin: " + jtsMinX + " xMax: " + jtsMaxX + " yMin: " + jtsMinY + " yMax: " + jtsMaxY;
        String blResult = "xMin: " + blMinX + " xMax: " + blMaxX + " yMin: " + blMinY + " yMax: " + blMaxY;
        boolean comparison;

        if (compareMaxX && compareMaxY && compareMinX && compareMinY) {
            comparison = true;
        } else {
            comparison = false;
        }
        this.output(geomType, testName, jtsResult, blResult, comparison);
    }

    /**
     * Calculates the intersection point with a line
     *
     * @param geomType
     * @param poly
     * @param ga
     */
    public void polyIntersectLine(int geomType, Polygon poly, GArea ga) {
        LOGGER.log(Level.INFO, this.logStart);
        //create fictive lines to intersect with
        Coordinate[] lineCoords = new Coordinate[]{poly.getCentroid().getCoordinate(), new Coordinate(10, 10)};
        LineString fictiveLine = new GeometryFactory().createLineString(lineCoords);

        String testName = "Polygon Linienverschnitt";
        String jtsResult;
        String blResult = "-";
        boolean comparison = false;

        //no method for bl to calculate intersection point between line and polygon
        //Intersection is a line so i have to get the point.
        if (poly.intersection(fictiveLine) instanceof MultiLineString) {
            jtsResult = "Mehrere Schnittpunkte";
        } else {
            LineString jtsIntersection = (LineString) poly.intersection(fictiveLine);
            Point jtsIntersectionPoint = jtsIntersection.getStartPoint();

            double jtsIntersectionX = jtsIntersectionPoint.getX();
            double jtsIntersectionY = jtsIntersectionPoint.getY();

            jtsResult = "x: " + jtsIntersectionX + " y: " + jtsIntersectionY;
        }

        this.output(geomType, testName, jtsResult, blResult, comparison);
    }

    /**
     * Checks if the fictive point is inside, outside or on the line of the
     * polygon
     *
     * @param geomType
     * @param poly
     * @param ga
     * @param fictivePoint
     * @param fictiveGPoint
     */
    public void polyIntersectPoint(int geomType, Polygon poly, GArea ga, Point fictivePoint, GPoint fictiveGPoint) {
        LOGGER.log(Level.INFO, this.logStart);

        boolean jtsPOnLine = poly.touches(fictivePoint);
        boolean jtsIntersectP = poly.contains(fictivePoint);
        int blIntersectP = ga.isInside(fictiveGPoint, null);

        String testName = "Polygon Punktverschnitt";
        String jtsResult = null;
        String blResult = null;
        boolean comparison;

        //Vergleich einfach mit String : Inside Outside onLine
        if (jtsIntersectP) {
            jtsResult = "Innerhalb";
        } else if (!jtsIntersectP) {
            jtsResult = "Ausserhalb";
        } else if (jtsPOnLine) {
            jtsResult = "Auf Linie";
        }

        switch (blIntersectP) {
            case 1:
                blResult = "Innerhalb";
                break;
            case -1:
                blResult = "Ausserhalb";
                break;
            case 0:
                blResult = "Auf Linie";
                break;
            default:
                break;
        }

        if (jtsResult.equals(blResult)) {
            comparison = true;
        } else {
            comparison = false;
        }
        this.output(geomType, testName, jtsResult, blResult, comparison);
    }

    /**
     * determines the nearest Vertex
     *
     * @param geomType
     * @param poly
     * @param ga
     */
    public void polyDetermineNearestVertex(int geomType, Polygon poly, GArea ga) {
        LOGGER.log(Level.INFO, this.logStart);
        //create fictive points on Line of polygon        
        Coordinate[] coordinatesVertex = poly.getCoordinates();
        Coordinate[] coordinatesLine = new Coordinate[]{coordinatesVertex[0], coordinatesVertex[1]};
        LineString line = new GeometryFactory().createLineString(coordinatesLine);
        Point fictivePoint = new GeometryFactory().createPoint(new Coordinate((line.getStartPoint().getX() + line.getEndPoint().getX()) / 2, (line.getStartPoint().getY() + line.getEndPoint().getY()) / 2));

        GBorder border = (ga.getBorder(0));
        GPoint[] points = border.getPointVector();
        GPoint p1 = points[0];
        GPoint p2 = points[1];
        GPoint fictiveGPoint = new GPoint("", (p1.x() + p2.x()) / 2, (p1.y() + p2.y()) / 2);

        //compute nearest vertex
        //Coordinate jtsVertex = point.getCoordinate();
        Coordinate[] jtsVertex = DistanceOp.nearestPoints(poly, fictivePoint);
        GPoint blVertex = ga.getNearestVertex(fictiveGPoint, 0);

        double jtsVertexX = jtsVertex[0].x;
        double jtsVertexY = jtsVertex[0].y;
        double blVertexX = blVertex.x();
        double blVertexY = blVertex.y();

        String testName = "Polygon nächster Stützpunkt";
        String jtsResult = "x: " + jtsVertexX + " " + "y: " + jtsVertexY;
        String blResult = "x: " + blVertexX + " " + "y: " + blVertexY;
        boolean comparison;

        if (jtsVertexX == blVertexX && jtsVertexY == blVertexY) {
            comparison = true;
        } else {
            comparison = false;
        }

        this.output(geomType, testName, jtsResult, blResult, comparison);
    }

    /**
     * Check if calculated centroid is inside of the area
     *
     * @param geomType
     * @param poly
     * @param ga
     */
    public void polyCheckCentroidInArea(int geomType, Polygon poly, GArea ga) {
        LOGGER.log(Level.INFO, this.logStart);
        //compute centroid
        Point centroid = poly.getCentroid();
        GPoint gCentroid = ga.getCentroid(0);

        String testName = "Polygon Centroid innerhalb";
        String jtsResult;
        String blResult;
        boolean comparison;

        //checks poly if it contains computed centroid
        if (poly.contains(centroid)) {
            jtsResult = "Innerhalb";
        } else {
            jtsResult = "Ausserhalb";
        }

        if (ga.isInside(gCentroid, null) == 1) {
            blResult = "Innerhalb";
        } else {
            blResult = "Ausserhalb";
        }

        if (jtsResult.equals(blResult)) {
            comparison = true;
        } else {
            comparison = false;
        }

        this.output(geomType, testName, jtsResult, blResult, comparison);

    }

    /**
     * Count Vertex
     *
     * @param geomType
     * @param poly
     * @param ga
     */
    public void polyCountVertex(int geomType, Polygon poly, GArea ga) {
        LOGGER.log(Level.INFO, this.logStart);
        //count the points from gBorder
        GPoint[] borderPoints = ga.getBorder(0).getPointVector();

        int jtsCountVertex = poly.getNumPoints();
        int blCountVertex = borderPoints.length;

        String testName = "Polygon Anzahl Stützpunkte";
        String jtsResult = Integer.toString(jtsCountVertex);
        String blResult = Integer.toString(blCountVertex);
        boolean comparison;

        if (jtsCountVertex == blCountVertex) {
            comparison = true;
        } else {
            comparison = false;
        }

        this.output(geomType, testName, jtsResult, blResult, comparison);
    }

    /**
     * Method for the output
     *
     * @param geomType
     * @param testName
     * @param jtsResult
     * @param blResult
     * @param compare
     */
    public void output(int geomType, String testName, String jtsResult, String blResult, boolean compare) {
        String comparison;

        System.out.println(testName);
        System.out.println("JTS:" + jtsResult);
        System.out.println("BL: " + blResult);
        if (compare == true) {
            comparison = "Gleiches Ergebnis";
            System.out.println(comparison + "\n");

        } else {
            comparison = "Ungleiches Ergebnis";
            System.out.println(comparison + "\n");
        }

        //saving data to map 
        switch (geomType) {
            //line geometrys
            case 1:
                List<String> resultListLine = new ArrayList<>();
                resultListLine.add(testName);
                resultListLine.add(jtsResult);
                resultListLine.add(blResult);
                resultListLine.add(comparison);
                Map<String, List<String>> objectMapLine = new HashMap<>();
                objectMapLine.put(this.objectId, resultListLine);
                this.listLine.add(objectMapLine);
                break;
            //polygon geometrys    
            case 2:
                List<String> resultListPoly = new ArrayList<>();
                resultListPoly.add(testName);
                resultListPoly.add(jtsResult);
                resultListPoly.add(blResult);
                resultListPoly.add(comparison);
                Map<String, List<String>> objectMapPoly = new HashMap<>();
                objectMapPoly.put(this.objectId, resultListPoly);
                this.listPoly.add(objectMapPoly);

                break;
            //polygonWArc geometrys
            case 3:
                List<String> resultListPolyWArc = new ArrayList<>();
                resultListPolyWArc.add(testName);
                resultListPolyWArc.add(jtsResult);
                resultListPolyWArc.add(blResult);
                resultListPolyWArc.add(comparison);
                Map<String, List<String>> objectMapPolyWArc = new HashMap<>();
                objectMapPolyWArc.put(this.objectId, resultListPolyWArc);
                this.listPolyWArc.add(objectMapPolyWArc);

                break;
            default:
                LOGGER.log(Level.WARNING, "Error with the geomType. Should be 1 / 2  or 3. geomType: {0}", geomType);
                break;
        }

        LOGGER.log(Level.INFO, this.logEnd);
    }

    /**
     * calculates the intersectionPoint for BL
     *
     * @param s1
     * @param e1
     * @param s2
     * @param e2
     * @return IntersectionPoint
     */
    public GPoint blCutLine(GPoint s1, GPoint e1, GPoint s2, GPoint e2) {
        GPoint[] resultArray = new GPoint[2];

        //calculates intersection point
        LXL.straightXstraight(resultArray, s1, e1, s2, e2, 0.0);
        return resultArray[0];
    }

    /**
     * @return resultList line geometries
     */
    public List<Map<String, List<String>>> getListLine() {
        return listLine;
    }

    /**
     *
     * @return resultList poly geometries
     */
    public List<Map<String, List<String>>> getListPoly() {
        return listPoly;
    }

    /**
     *
     * @return resultList poly w arc geometries
     */
    public List<Map<String, List<String>>> getListPolyWArc() {
        return listPolyWArc;
    }

    /**
     * logs the coordinate informations of the tested objects
     *
     * @param jtsGeom
     * @param blGeom
     * @param fictivePoint
     * @param fictiveGPoint
     */
    public void logCoordinateInfos(Geometry jtsGeom, Geom blGeom, Point fictivePoint, GPoint fictiveGPoint) {

        if (jtsGeom instanceof LineString && blGeom instanceof GLine) {

            LineString jtsLine = (LineString) jtsGeom;
            GLine blLine = (GLine) blGeom;

            LOGGER.log(Level.INFO, "JTS: StartPoint X: ({0})", Double.toString(jtsLine.getStartPoint().getX()));
            LOGGER.log(Level.INFO, "JTS: StartPoint Y: ({0})", Double.toString(jtsLine.getStartPoint().getY()));
            LOGGER.log(Level.INFO, "JTS: EndPoint X: ({0})", Double.toString(jtsLine.getEndPoint().getX()));
            LOGGER.log(Level.INFO, "JTS: EndPoint Y: ({0})", Double.toString(jtsLine.getEndPoint().getY()));

            LOGGER.log(Level.INFO, "BL: StartPoint X: ({0})", Double.toString(blLine.getStart().x()));
            LOGGER.log(Level.INFO, "BL: StartPoint Y: ({0})", Double.toString(blLine.getStart().y()));
            LOGGER.log(Level.INFO, "BL: EndPoint X: ({0})", Double.toString(blLine.getEnd().x()));
            LOGGER.log(Level.INFO, "BL: EndPoint Y: ({0})", Double.toString(blLine.getEnd().x()));

            LOGGER.log(Level.INFO, "JTS: fictivePoint X: ({0})", Double.toString(fictivePoint.getX()));
            LOGGER.log(Level.INFO, "JTS: fictivePoint Y: ({0})", Double.toString(fictivePoint.getY()));

            LOGGER.log(Level.INFO, "BL: fictivePoint X: ({0})", Double.toString(fictiveGPoint.x()));
            LOGGER.log(Level.INFO, "BL: fictivePoint Y: ({0})", Double.toString(fictiveGPoint.y()));

        } else if (jtsGeom instanceof Polygon && blGeom instanceof GArea) {

            Polygon jtsPoly = (Polygon) jtsGeom;
            GArea blPoly = (GArea) blGeom;

            Coordinate[] jtsCoordinates = jtsPoly.getCoordinates();
            LOGGER.log(Level.INFO, "JTS Polygon Coordinates: ");
            int countCoordinates = 0;
            for (Coordinate c : jtsCoordinates) {
                countCoordinates++;
                LOGGER.log(Level.INFO, "Coordinate X{0}: {1}", new Object[]{countCoordinates, Double.toString(c.x)});
                LOGGER.log(Level.INFO, "Coordinate Y{0}: {1}", new Object[]{countCoordinates, Double.toString(c.y)});
            }
            LOGGER.warning("no suitable method to get all coordinates from bl GArea");

        } else {
            LOGGER.severe("Geometry is not Polygon or Line");
        }

    }

}
